package com.rogerpraca.mobilight;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import android.view.View.OnClickListener;

public class MobilightActivity extends Activity {

	private ToggleButton torchTBtn, blinkerTBtn;
	public static Camera cam=null;
	private Handler blinkerHandler = new Handler();
	private Runnable runblinker = null;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.activity_mobilight);
		
		cam = Camera.open();     
		Parameters p = cam.getParameters();
		p.setFlashMode(Parameters.FLASH_MODE_TORCH);
		cam.setParameters(p);
		
		runblinker= new Runnable() {
			private int flag=0;

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(flag==0){
					flag=1;
					cam.startPreview();
				}
				else{
					flag=0;
					cam.stopPreview();
					
				}
				blinkerHandler.postDelayed(this, 1000);
			}
			
		};
		
		addListenersOnElements();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.mobilight, menu);
		return true;
	}
	
	protected void addListenersOnElements(){
		torchTBtn = (ToggleButton) findViewById(R.id.torchTBtn );
		blinkerTBtn = (ToggleButton) findViewById(R.id.blinkerTBtn);
		
		
		OnCheckedChangeListener torchListener = new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(torchTBtn.isChecked()){
					blinkerTBtn.setEnabled(false);
					
					cam.startPreview();
				}
				else{
					cam.stopPreview();
					blinkerTBtn.setEnabled(true);
					
				}
				
			}
		};
		OnCheckedChangeListener blinkerListener = new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(blinkerTBtn.isChecked()){
					torchTBtn.setEnabled(false);
					blinkerHandler.removeCallbacks(runblinker);
		            blinkerHandler.postDelayed(runblinker, 100);
					
				}
				else{
					cam.stopPreview();
					torchTBtn.setEnabled(true);
					blinkerHandler.removeCallbacks(runblinker);
				}
				
			}
		};
		
		cam.stopPreview();		
		torchTBtn.setOnCheckedChangeListener(torchListener);
		blinkerTBtn.setOnCheckedChangeListener(blinkerListener);
	}
	

}
